package com.example.sandra.dora.controller;

import com.example.sandra.dora.pojos.Alimento;

import java.util.ArrayList;

public interface MyCallBackAlimentos {
    void onCallBackAlimentos(ArrayList<Alimento> list);
}
