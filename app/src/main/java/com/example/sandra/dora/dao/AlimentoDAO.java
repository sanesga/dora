package com.example.sandra.dora.dao;

import android.support.annotation.NonNull;

import com.example.sandra.dora.controller.MyCallBackAlimentos;
import com.example.sandra.dora.controller.MyCallBackUnAlimento;
import com.example.sandra.dora.pojos.Alimento;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AlimentoDAO {

    private DatabaseReference referencia;
    private FirebaseUser user;
    private String userId;
    private ArrayList<Lugar> placesList;
    private Lugar l;
    ArrayList<Almacenamiento> listaAlmacenamientos;
    ArrayList<Alimento> listaAlimentos;
    private Alimento a=null;

    public AlimentoDAO() {

        user = FirebaseAuth.getInstance().getCurrentUser(); //obtenemos el usuario logeado
        referencia = FirebaseDatabase.getInstance().getReference(); //otenemos la base de datos que estamos utilizando
        userId = user.getUid(); //obtenemos el id del usuario logeado para guardar los datos en su bd

        placesList = new ArrayList<>();
        listaAlimentos = new ArrayList<>();
    }

    public void add(Alimento a) {
        //guardamos el alimento con la clave primaria del codigo de barras
        referencia.child("Usuarios").child(userId).child("Alimentos").child(String.valueOf(a.getCodigoBarras())).setValue(a);
    }

    public void addCurrentList(Alimento a) {
        //guardamos el alimento con la clave primaria del codigo de barras
        referencia.child("Usuarios").child(userId).child("AlimentosActuales").child(String.valueOf(a.getCodigoBarras())).setValue(a);
    }

    public void getAllFood(final MyCallBackAlimentos myCallBack) {

        //referencia es la instancia a nuestra base de datos
        DatabaseReference userRef = referencia.child("Usuarios").child(userId);

        userRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot etiquetaAlimentos = dataSnapshot.child("Alimentos");

                for (DataSnapshot alimento : etiquetaAlimentos.getChildren()) {

                    a = new Alimento();

                    a.setCodigoBarras(alimento.child("codigoBarras").getValue().toString());
                    a.setNombre(alimento.child("nombre").getValue().toString());
                    a.setDescripcion(alimento.child("descripcion").getValue().toString());
                    a.setPrecio(Double.parseDouble(alimento.child("precio").getValue().toString()));
                    a.setFoto(alimento.child("foto").getValue().toString());

                    listaAlimentos.add(a);

                }//fin del for

                //usamos un interface para guardar los datos a través de él, ya que la llamada a firebase es asíncrona y no podemos devolver los datos a través de este método, porque se intentarán enviar antes de que estén listos.
                myCallBack.onCallBackAlimentos(listaAlimentos);

            } //fin del ondatachange

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

    }

    public void getCurrentFood(final MyCallBackAlimentos myCallBack) {

        //referencia es la instancia a nuestra base de datos
        DatabaseReference userRef = referencia.child("Usuarios").child(userId);

        userRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot etiquetaAlimentos = dataSnapshot.child("AlimentosActuales");

                for (DataSnapshot alimento : etiquetaAlimentos.getChildren()) {

                    a = new Alimento();

                    a.setCodigoBarras(alimento.child("codigoBarras").getValue().toString());
                    a.setNombre(alimento.child("nombre").getValue().toString());
                    a.setDescripcion(alimento.child("descripcion").getValue().toString());
                    a.setPrecio(Double.parseDouble(alimento.child("precio").getValue().toString()));
                    a.setFoto(alimento.child("foto").getValue().toString());

                    listaAlimentos.add(a);

                }//fin del for

                //usamos un interface para guardar los datos a través de él, ya que la llamada a firebase es asíncrona y no podemos devolver los datos a través de este método, porque se intentarán enviar antes de que estén listos.
                myCallBack.onCallBackAlimentos(listaAlimentos);

            } //fin del ondatachange

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

    }

    public void getOne(final String barcode, final MyCallBackUnAlimento myCallBack) {


        DatabaseReference userRef = referencia.child("Usuarios").child(userId);

        userRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { //este dataSnapshot es el usuario logeado

                if (dataSnapshot.hasChild("Alimentos")) {//si tiene la etiqueta alimentos

                    if (dataSnapshot.child("Alimentos").hasChild(barcode)) { //si tiene un alimento con ese código de barras

                        DataSnapshot alimento = dataSnapshot.child("Alimentos").child(barcode);

                        String nombre = alimento.child("nombre").getValue().toString();
                        String descripcion = alimento.child("descripcion").getValue().toString();
                        String precio = alimento.child("precio").getValue().toString();
                        String foto = alimento.child("foto").getValue().toString();
                        String codigoBarras = alimento.child("codigoBarras").getValue().toString();

                        a = new Alimento();
                        a.setFoto(foto);
                        a.setNombre(nombre);
                        a.setDescripcion(descripcion);
                        a.setPrecio(Double.parseDouble(precio));
                        a.setCodigoBarras(codigoBarras);
                    } else {
                        a = new Alimento();  //si el alimento no está en la lista, no hacemos nada y devolverá el alimento a null
                        a = null;
                    }
                } else {
                    a = new Alimento();
                    a = null;
                }
                myCallBack.onCallBackUnAlimento(a);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("Error leyendo la base de datos");
            }
        });
    }

    public void deleteFood(String barcode) {
        referencia.child("Usuarios").child(userId).child("AlimentosActuales").child(barcode).removeValue();
    }
}
