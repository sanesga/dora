package com.example.sandra.dora.model;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.dao.AlimentoDAO;
import com.example.sandra.dora.pojos.Alimento;

public class DetalleAlimento extends AppCompatActivity implements View.OnClickListener {

    private Alimento a;
    private TextView tvNombreAlimento, tvDescripcionAlimento, tvPrecioAlimento;
    private ImageView ivFotoAlimento, ivBorrarAlimento;
    private Bitmap foto;
    private AlimentoDAO alimentoDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_alimento);

        //*******************************--REFERENCIAS--********************************************
        tvNombreAlimento=findViewById(R.id.tvNombreAlimento);
        tvDescripcionAlimento=findViewById(R.id.tvDescripcionAlimento);
        tvPrecioAlimento=findViewById(R.id.tvPrecioAlimento);
        ivBorrarAlimento= findViewById(R.id.botonBorrarAlimento);
        ivFotoAlimento = findViewById(R.id.ivFotoAlimento);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        ivBorrarAlimento.setOnClickListener(this);
        //******************************************************************************************

        //recogemos el alimento enviado
        Bundle parametros = this.getIntent().getExtras();
        if (parametros != null) {
            a= (Alimento) getIntent().getExtras().get("alimento");
        }

        //colocamos sus atributos en sus respectivos campos
        tvNombreAlimento.setText(a.getNombre());
        tvDescripcionAlimento.setText(a.getDescripcion());
        tvPrecioAlimento.setText(String.valueOf(a.getPrecio()));

        foto=a.decodeImage();
        ivFotoAlimento.setImageBitmap(foto);

    }

    @Override
    public void onClick(View v) {
        //mostramos un show dialog para confirmar
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("¿Está seguro de querer eliminar el alimento?");
        dialog.setCancelable(false);
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar();
            }
        });
        dialog.show();
    }

    public void aceptar() {
        alimentoDAO = new AlimentoDAO();
        alimentoDAO.deleteFood(a.getCodigoBarras());
        Toast.makeText(this, "Alimento eliminado de la lista de la compra", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void cancelar() {
        //no hacemos nada
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}


