package com.example.sandra.dora.adaptador;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.model.Login;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeMailDialog extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText etNewEmail, etNewEmailRepeat;
        final TextView tvErrorMessage;

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_change_email_dialog, null);

        //*************************************REFERENCIAS******************************************
        etNewEmail = (EditText) v.findViewById(R.id.etnewemail);
        etNewEmailRepeat = (EditText) v.findViewById(R.id.etnewemailrepeat);
        tvErrorMessage = (TextView) v.findViewById(R.id.errorMessageMail);
        //******************************************************************************************

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(v)
                //.setTitle("mi dialogo")
                .setPositiveButton(R.string.botonGuardar, null) //Set to null. We override the onclick
                .setNegativeButton(R.string.botonCancelar, null)
                .create();


        //SOBREESCRIBIMOS EL BOTÓN DE ACEPTAR, PARA PODER CORROBORAR QUE LOS EMAILS COINCIDEN Y MOSTRAR ERROR SI NECESARIO ANTES DE QUE SE CIERRE EL DIALOG
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view) {

                        String mail = etNewEmail.getText().toString();
                        String mailRepeat = etNewEmailRepeat.getText().toString();


                        if(mail.equals("") || mailRepeat.equals("")){    //VALIDAMOS QUE NO HAYA CAMPOS VACÍOS
                            tvErrorMessage.setText("Todos los campos son requeridos");
                            if(mail.equals("")){
                                etNewEmail.setTextColor(R.color.rojo);
                            }
                            if(mailRepeat.equals("")){
                                etNewEmailRepeat.setTextColor(R.color.rojo);
                            }
                        }else { //SI LOS DOS CAMPOS SE HAN RELLENADO

                            //VALIDAMOS QUE EL CORREO TENGA FORMATO DE CORREO ELECTRÓNICO
                            String emailPattern = String.valueOf(R.string.emailPattern);

                            //compilamos el pattern
                            Pattern pattern = Pattern.compile(emailPattern);

                            //comparamos formatos
                                Matcher matcher = pattern.matcher(mail);
                                if (matcher.matches()) { //SI EL PRIMER MAIL INTRODUCIDO TIENE EL FORMATO CORRECTO, COMO EL OTRO DEBERÁ ESTAR IGUAL, TAMBIÉN ESTARÁ BIEN

                                    if (mail.equals(mailRepeat)) {

                                        //SI LOS DOS EMAILS COINCIDEN, LO CAMBIAMOS Y VOLVEMOS A LA VENTANA DE LOGIN
                                        UserController dbc = new UserController();
                                        dbc.changeEmail(mail);

                                        Toast.makeText(getContext(), R.string.emailCambiado, Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();

                                        Intent i = new Intent(getContext(), Login.class);
                                        startActivity(i);

                                    } else {
                                        //SI NO COINCIDEN, PONEMOS UN MENSAJE DE ERROR
                                        tvErrorMessage.setText(R.string.errorCambioEmail);
                                    }

                                } else {
                                    tvErrorMessage.setText(R.string.errorFormatoEmail);
                                }
                        }
                    }
                });
            }
        });
        dialog.show();

        return dialog;
    }
}
