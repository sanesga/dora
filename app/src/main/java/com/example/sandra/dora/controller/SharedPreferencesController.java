package com.example.sandra.dora.controller;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesController {

    private SharedPreferences prefs;
    private String placeName;
    private SharedPreferences.Editor editor;


    public SharedPreferencesController(Context c) { //al hacer el new le pasaremos el getApplicationContext()
        //REFERENCIA DEL SHARED PREFERENCE (MODO LECTURA)
        prefs = c.getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        //REFERENCIA DEL SHARED PREFERENCE (MODO ESCRITURA)
        editor = c.getSharedPreferences("Preferencias", Context.MODE_PRIVATE).edit();
    }

    public void savePlaceName(String s){
        editor.putString("nombrelugar", s);
        editor.commit();
    }
    public String getPlaceName() {
        //LA PRIMERA VEZ ESTARÁ VACÍO
        placeName = prefs.getString("nombrelugar", "");
        return placeName;
    }
    public void resetPlaceData(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("nombrelugar", "");
        editor.putFloat("latitudLugar", 0);
        editor.putFloat("longitudLugar", 0);
        editor.commit();
    }
    public void savePlaceCoordinates(double longitude, double latitude){
       //PASAMOS DE DOUBLE A FLOAT PARA PODER GUARDAR LOS DATOS EN SHARED PREFERENCES (no se pueden guardar como double)
        editor.putFloat("latitudLugar", (float)latitude);
        editor.putFloat("longitudLugar", (float)longitude);
        editor.commit();
    }
    public Float getPlaceLatitude() {
        //recuperamos los datos como float
        return prefs.getFloat("latitudLugar",0);
    }
    public Float getPlaceLongitude() {
        //recuperamos los datos como float
        return prefs.getFloat("longitudLugar",0);
    }
    public void saveNameStorage(String storageName){ //guardamos el nombre del almacenamiento seleccionado
        editor.putString("nombrealmacenamiento", storageName);
        editor.commit();
    }
    public String getNameStorage( ){ //recuperamos el nombre del almacenamiento seleccionado
        return prefs.getString("nombrealmacenamiento","");
    }

    public void saveCameraSignal( ){ //desde el show dialog, guardamos un 1 si queremos acceder a la cámara y un 2 si accedemos a la galería
        editor.putInt("signal", 1);
        editor.commit();
    }
    public void saveGallerySignal( ){
        editor.putInt("signal", 2);
        editor.commit();
    }
    public int getImageSignal( ){ //recuperamos el nombre del almacenamiento seleccionado
        return prefs.getInt("signal",0);
    }
    public void savePlaceId(String id){
        editor.putString("placeId", id);
        editor.commit();
    }
    public String getPlaceId(){
        return prefs.getString("placeId","");
    }
    public void resetPlaceId(){
        editor.putString("placeId","");
        editor.commit();
    }
    public void saveCar(double longitud, double latitud){
        editor.putFloat("longitudVehiculo", (float) longitud);
        editor.putFloat("latitudVehiculo", (float) latitud);
        editor.commit();
    }
    public Float getCarLatitude() {
        //recuperamos los datos como float
        return prefs.getFloat("latitudVehiculo",0);
    }
    public Float getCarLongitude() {
        //recuperamos los datos como float
        return prefs.getFloat("longitudVehiculo",0);
    }
    public void ChangeLanguage(){

    }
}
