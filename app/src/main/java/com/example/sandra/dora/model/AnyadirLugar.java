package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Lugar;

public class AnyadirLugar extends AppCompatActivity implements View.OnClickListener {

    private EditText etPlaceName;
    private TextView tvAnyadirCoordenadas;
    private Button addPlaceButton;
    private Lugar lugar;
    private String nombre;
    private float longitud, latitud;
    private ImageView coordinatesCheck;
    private SharedPreferencesController spc;
    private LugarDAO lugarDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_lugar);

        //*******************************--REFERENCIAS--********************************************
        tvAnyadirCoordenadas = findViewById(R.id.etDescriptionFood);
        addPlaceButton = findViewById(R.id.addPlaceButton);
        etPlaceName = findViewById(R.id.etNameFood);
        coordinatesCheck = findViewById(R.id.coordinatesPlace);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        tvAnyadirCoordenadas.setOnClickListener(this);
        addPlaceButton.setOnClickListener(this);
        //******************************************************************************************
    }

    @Override
    public void onClick(View v) {
        Intent i = null;

        switch (v.getId()) { //AÑADIR COORDENADAS
            case R.id.etDescriptionFood: //CUANDO DAMOS AL TEXT VIEW DE AÑADIR COORDENADAS

                nombre = etPlaceName.getText().toString();  //RECUPERAMOS LOS DATOS DEL LUGAR PARA CREAR EL OBJETO
                spc.savePlaceName(nombre);  //GUARDAMOS LOS DATOS RECUPERADOS EN SHARED PREFERENCES
                i = new Intent(this, Ubicacion.class); //ENVIAMOS A LA PANTALLA DE MAPA
                startActivity(i);
                break;
            case R.id.addPlaceButton: //CUANDO DAMOS AL BOTÓN GUARDAR
                //RECUPERAMOS LOS DATOS DEL LUGAR PARA CREAR EL OBJETO
                nombre = etPlaceName.getText().toString();

                //obtenemos las coordenadas del shared preferences
                latitud=spc.getPlaceLatitude();
                longitud = spc.getPlaceLongitude();

                if(!nombre.equals("") && latitud!=0 && longitud!=0){ //si hemos escrito un nombre
                    //CREO EL OBJETO LUGAR
                    lugar = new Lugar(nombre);

                    //SETEAMOS LA UBICACIÓN
                    lugar.setLongitud(longitud);
                    lugar.setLatitud(latitud);

                    //GUARDAMOS LUGAR EN LA BASE DE DATOS
                    lugarDAO = new LugarDAO();
                    lugarDAO.add(lugar);

                    //CUANDO AÑADIMOS A LA BASE DE DATOS RESETEAMOS EL SHARED PREFERENCES
                    spc.resetPlaceData();

                    //RESETEAMOS LOS CAMPOS
                    etPlaceName.setText("");
                    coordinatesCheck.setVisibility(View.INVISIBLE);

                    Toast.makeText(this, "Lugar guardado", Toast.LENGTH_SHORT).show();
                    i = new Intent(this, MenuPrincipal.class);
                    startActivity(i);
                    finish();

                }else{
                    Toast.makeText(this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //cuando volvemos a esta actividad, después de la actividad ubicación, ya tenemos todos los datos guardados en shared preferences
        //recuperamos datos de shared preferences
        //RECUPERAMOS LOS DATOS DEL SHARED PREFERENCES

        spc = new SharedPreferencesController(getApplicationContext()); //hacemos el new del shared preferences pasándole el contexto

        //recuperamos los datos del lugar
        nombre = spc.getPlaceName();
        latitud = spc.getPlaceLatitude();
        longitud = spc.getPlaceLongitude();

        etPlaceName.setText(nombre); //mostramos el nombre que hemos introducido (la primera vez estará vacío, las siguientes será el nombre que hemos guardado, que se recupera al volver a la pantalla desde el mapa, para no volverlo a introducir)

        if (longitud != 0.0) { //hacemos visible el check verde
            coordinatesCheck.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() { //HACIA ATRÁS
        super.onBackPressed();
        //CUANDO VAMOS ATRÁS RESETEAMOS EL SHARED PREFERENCES
        spc.resetPlaceData();

        //Y QUITAMOS EL CHECK VERDE
        coordinatesCheck.setVisibility(View.INVISIBLE);

        finish();
    }

}