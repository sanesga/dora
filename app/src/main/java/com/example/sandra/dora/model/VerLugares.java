package com.example.sandra.dora.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.fragments.fragment_almacenamientos;
import com.example.sandra.dora.R;
import com.example.sandra.dora.fragments.LugaresListener;
import com.example.sandra.dora.pojos.Lugar;

import java.util.ArrayList;


public class VerLugares extends AppCompatActivity implements LugaresListener {

    private com.example.sandra.dora.fragments.fragment_lugares fragment_lugares;
    private ArrayList<Lugar> placesList;
    private Toolbar toolbar;
    private UserController uc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_lugares);

        //*******************************--REFERENCIAS--********************************************
        //referencia al fragment para poder hacer click enviando a lugares listener
        fragment_lugares = (com.example.sandra.dora.fragments.fragment_lugares) getSupportFragmentManager().findFragmentById(R.id.fragmentLugares);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        //llama al metodo setcorreoslistener pasándole esta actividad (este método está en activity_fragment_lugares)
        fragment_lugares.setLugaresListener(this);
        //******************************************************************************************

        //----------TOOLBAR----------
        setSupportActionBar(toolbar);
        //--------------------------
    }

    public void actionMenu(View view) {

        Intent i = null;

        switch (view.getTag().toString()) {
            case "ajustes":
                i = new Intent(this, Configuracion.class);
                break;
            case "user":
                //aqui llamaremos al metodo getUser y obtendremos el nombre del usuario
                uc = new UserController();
                String userMail;
                userMail = uc.getUserData();
                i = new Intent(this, Account.class);
                i.putExtra("mailAccount", userMail);
                break;
            case "home":
                i = new Intent(this, MenuPrincipal.class);
                break;
        }
        startActivity(i);
    }

    @Override
    public void onLugarSeleccionado(Lugar l) {
        boolean hayDetalle = (getSupportFragmentManager().findFragmentById(R.id.fragmentDetalle) != null);

        if (hayDetalle) { //VISTA TABLET
            ((fragment_almacenamientos) getSupportFragmentManager().findFragmentById(R.id.fragmentDetalle)).mostrarDetalle(l);

        } else { //VISTA MÓVIL
            Intent i = new Intent(this, VerAlmacenamientos.class);
            i.putExtra("lugar", l);
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {
    }
}
