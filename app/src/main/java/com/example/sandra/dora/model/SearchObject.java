package com.example.sandra.dora.model;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.adaptador.MiArrayAdapterGridViewObjetos;
import com.example.sandra.dora.controller.MyCallBack;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;
import java.util.ArrayList;
import java.util.Locale;

public class SearchObject extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private EditText etSearchObject;
    private GridView gvResults;
    private String nombreObjetoABuscar, palabraDicha, resultado;
    private ImageView ivSearch;
    private MiArrayAdapterGridViewObjetos<Objeto> adapter;
    private ArrayList<Objeto> listaObjetos = new ArrayList<>();
    private ArrayList<Objeto> resultadoBusqueda = new ArrayList<>();
    private LugarDAO lugarDAO;
    private TextView tvObjectNumber;
    private TextToSpeech tts;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private FloatingActionButton fab, fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //*******************************--REFERENCIAS--********************************************
        etSearchObject = findViewById(R.id.etSearchObject);
        gvResults = findViewById(R.id.gvResults);
        tvObjectNumber = findViewById(R.id.tvTotalResultados);
        ivSearch = findViewById(R.id.ivSearch);
        fab = findViewById(R.id.fabMicro); //este es el botón que dará el mensaje de bienvenida
        fab2= findViewById(R.id.fabMicro2); //este es el botón que escucha
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        ivSearch.setOnClickListener(this);
        gvResults.setOnItemClickListener(this);
        fab.setOnClickListener(this);
        fab2.setOnClickListener(this);
        //******************************************************************************************

        InitializeTextToSpeech(); //inicializamos aquí el asistente de voz sin mensaje, para poder cerrarlo cuando demos al botón atrás (si lo inicializamos al dar al botón del micro, cuando intente cerrarlo, dará error por no tener el new hecho).

        //ESCONDEMOS BOTÓN DE HABLAR
        fab2.hide();

        //obtenemos la lista de objetos guardados
        lugarDAO = new LugarDAO();
        lugarDAO.getAll(new MyCallBack() {

            @Override
            public void onCallBackLugar(ArrayList<Lugar> list) {


               for (Lugar l : list) { //cada lugar
                    for (Almacenamiento a : l.getListaAlmacenamientos()) { //cada almacenamiento
                        for (Objeto o : a.getListaObjetos()) { //cada objeto
                            listaObjetos.add(o);
                            resultadoBusqueda.add(o);
                        }
                    }
                }
                //mostramos un texto de título
                tvObjectNumber.setText("Todos los objetos");
                //colocamos los objetos en el gridview
                adapter = new MiArrayAdapterGridViewObjetos<>(getApplicationContext(), resultadoBusqueda); //cargamos los objetos en resultado búsqueda para poder acceder a su detalle cuando hacemos click
                gvResults.setAdapter(adapter);
            }
        });
    }


    private void bienvenida() {
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (tts.getEngines().size() == 0) {
                    Toast.makeText(SearchObject.this, "Su dispositivo no tiene activado o no soporta el asistente de voz. Asegúrese de tener la aplicación de Google habilitada.", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Locale locale = new Locale("es", "ES");
                    tts.setLanguage(locale);
                    speak("Dime el nombre del objeto que quieres que busque");
                }
            }
        });
    }
    private void InitializeTextToSpeech() {
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
              //no hacemos nada
            }
        });
    }


    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {

            case R.id.ivSearch: //LUPA
                //vaciamos la búsqueda antes de realizar otra
                resultadoBusqueda.clear();

                //obtenemos el texto del edit text
                nombreObjetoABuscar = etSearchObject.getText().toString();

                //llamamos al método que buscará el objeto en el arraylist
               resultadoBusqueda= buscarObjeto(listaObjetos, nombreObjetoABuscar);

                //mostramos el número de objetos encontrados
                tvObjectNumber.setText(resultadoBusqueda.size() + " resultados");

                //colocamos el resultado de la búsqueda en el gridView
                adapter = new MiArrayAdapterGridViewObjetos<>(this, resultadoBusqueda);
                gvResults.setAdapter(adapter);

                break;

            case R.id.fabMicro: //hacemos click en el micro y nos da el mensaje de bienvenida
                bienvenida();
                fab.hide(); //escondemos el mensaje de bienvenida
                fab2.show(); //mostramos el de hablar
                break;
            case R.id.fabMicro2: //nos permite hablar
                escuchar();
                break;

        }
    }

    private ArrayList<Objeto> buscarObjeto(ArrayList<Objeto> listaObjetos, String nombreObjetoABuscar) {

        ArrayList<Objeto> resultadoBusqueda= new ArrayList<>();

        for (Objeto o : listaObjetos) {
            if (o.getNombre().equalsIgnoreCase(nombreObjetoABuscar)) {
                resultadoBusqueda.add(o);
            }
        }
        return resultadoBusqueda;
    }

    public void escuchar() { //cuando se hace click en el micrófono, llamamos a este método
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(SearchObject.this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(SearchObject.this,
                    Manifest.permission.RECORD_AUDIO)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(SearchObject.this,
                        new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

            if (i.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(i, 10);
            } else {
                Toast.makeText(this, "Su dispositivo no tiene activado o no soporta el asistente de voz. Asegúrese de tener la aplicación de Google habilitada.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10: //ESTO SE EJECUTA DESPUÉS DE HABLAR

                if (resultCode == RESULT_OK && data != null) {
                    //vaciamos la búsqueda antes de realizar otra
                    resultadoBusqueda.clear();
                    resultado="";

                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    //palabra dicha
                    palabraDicha=result.get(0);

                    //buscamos lo que ha detectado, en el array de objetos
                    resultadoBusqueda=buscarObjeto(listaObjetos,palabraDicha );

                    //mostramos el número de objetos encontrados
                    tvObjectNumber.setText(resultadoBusqueda.size() + " resultados");

                    //colocamos el resultado de la búsqueda en el gridView
                    adapter = new MiArrayAdapterGridViewObjetos<>(this, resultadoBusqueda);
                    gvResults.setAdapter(adapter);

                    //guardamos mensajes en el speaker
                    resultado="Buscando " + palabraDicha;
                    resultado+=". Encontrados " + resultadoBusqueda.size();

                    //nos dice lo que ha encontrado
                    for (Objeto o:resultadoBusqueda) {
                        int i=1;
                        resultado+=(". Número " + i + " "+ o.getNombre() + "con descripción: " + o.getDescripcion() + ". Se encuentra en almacenamiento " + o.getAlmacenamiento() + " del lugar " + o.getLugar());
                        i++;
                    }

                    //nos dice el resultado
                    speak(resultado);

                }
                break;
        }

    }

    private void speak(String message) {
        if (Build.VERSION.SDK_INT >= 21) {
            tts.speak(message, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        System.out.println("entra al onitemclick");
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        Objeto objetoSeleccionado=resultadoBusqueda.get(position);

        dialog.setTitle("INFORMACIÓN DEL OBJETO");
        StringBuilder message = new StringBuilder();
        message.append("Nombre: " + objetoSeleccionado.getNombre()+"\n") ;
        message.append("Descripción: " + objetoSeleccionado.getDescripcion()+"\n");
        message.append("Lugar: "+ objetoSeleccionado.getLugar()+ "\n");
        message.append("Almacenamiento: "+ objetoSeleccionado.getAlmacenamiento()+ "\n");
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(tts.isSpeaking()){
            tts.shutdown(); //paramos el asistente de voz al ir hacia atrás
        }

    }
}
