package com.example.sandra.dora.controller;

import com.example.sandra.dora.pojos.Lugar;

import java.util.ArrayList;

//interfaz para recuperar  una lista de lugares
public interface MyCallBack {
    void onCallBackLugar (ArrayList<Lugar> list);
}
