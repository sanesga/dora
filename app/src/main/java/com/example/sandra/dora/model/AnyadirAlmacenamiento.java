package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;

public class AnyadirAlmacenamiento extends AppCompatActivity implements View.OnClickListener {

    private EditText etAddStorage;
    private String nombreAlmacenamiento;
    private Almacenamiento a;
    private Button btnSaveStorage;
    private Lugar l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_almacenamiento);

        //*******************************--REFERENCIAS--********************************************
        etAddStorage = findViewById(R.id.etNameFood);
        btnSaveStorage = findViewById(R.id.btnSaveStorage);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        btnSaveStorage.setOnClickListener(this);
        //******************************************************************************************


        //recibimos el lugar donde almacenaremos las unidades de almacenamiento
        Bundle parametros = this.getIntent().getExtras();
        if (parametros != null) {
            l = (Lugar) getIntent().getExtras().get("lugar");
        }
    }

    @Override
    public void onClick(View v) { //GUARDAMOS EL ALMACENAMIENTO EN LA BASE DE DATOS

        nombreAlmacenamiento = etAddStorage.getText().toString();

        if(!nombreAlmacenamiento.equals("")){
            a = new Almacenamiento();

            a.setNombre(nombreAlmacenamiento);

            l.getListaAlmacenamientos().add(a);

            LugarDAO lugarDAO = new LugarDAO();
            lugarDAO.updatePlace(l);

            Toast.makeText(this, "Almacenamiento guardado", Toast.LENGTH_SHORT).show();

            etAddStorage.setText(""); //reseteamos el edit text

            Intent i = new Intent(this, VerAlmacenamientos.class); //llamo a lugares, para volver a entrar a detalle lugares y que la lista esté actualizada
            i.putExtra("lugar", l);
            startActivity(i);

            finish(); //es necesario hacer un finish, porque la pantalla anterior de ver detalle lugares se llama desde un fragment, es decir, se llama al método mostrarLugar al que le pasamos un lugar y desde esta pantalla no le pasamos un lugar y da error.
        }else{
            Toast.makeText(this, "Debe indicar un nombre", Toast.LENGTH_SHORT).show();
        }
    }
}
