package com.example.sandra.dora.pojos;

import java.io.Serializable;
import java.util.ArrayList;

public class Lugar implements Serializable {

    private String nombre;
    private double latitud;
    private double longitud;
    private String id;
    private ArrayList<Almacenamiento> listaAlmacenamientos;


    @Override
    public String toString() {
        return "Lugar{" + "nombre='" + nombre + '\'' + ", latitud=" + latitud + ", longitud=" + longitud + '}';
    }

    public Lugar() {
        listaAlmacenamientos = new ArrayList<>();
    }

    public Lugar(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Almacenamiento> getListaAlmacenamientos() {
        return listaAlmacenamientos;
    }

    public void setListaAlmacenamientos(ArrayList<Almacenamiento> listaAlmacenamientos) {
        this.listaAlmacenamientos = listaAlmacenamientos;
    }
}
