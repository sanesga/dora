package com.example.sandra.dora.pojos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class Alimento implements Serializable {

    private String codigoBarras;
    private String nombre;
    private String descripcion;
    private String foto;
    private double precio;

    public Alimento() {
    }

    @Override
    public String toString() {
        return "Alimento{" +
                "codigoBarras=" + codigoBarras +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", foto='" + foto + '\'' +
                ", precio=" + precio +
                '}';
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    public Bitmap decodeImage(){ //DECODIFICAMOS LA IMAGEN DE BASE64 A BITMAP
        byte[] decodedString = Base64.decode(this.getFoto(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        //Bitmap bitmap = Bitmap.createScaledBitmap(decodedByte, 130, 200, true); //le damos el tamaño que queremos
        return decodedByte;
    }
    public String encodeImage(Bitmap bitmap){ //CODIFICAMOS LA IMAGEN DE BITMAP A BASE64
        //convertimos el bitmap a string, para poder guardarlo en firebase
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }
}
