package com.example.sandra.dora.model;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.sandra.dora.R;

import java.util.Locale;

public class Bienvenida extends AppCompatActivity implements Runnable {

    private Thread hiloProgresBar = new Thread(this);
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);

        //*******************************--REFERENCIAS--********************************************
        progressBar = findViewById(R.id.progressBar);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************

        //******************************************************************************************

        //PROGRESSBAR----
        progressBar.setProgress(0);
        progressBar.setMax(100);

        //INICIAMOS NUEVO HILO DE EJECUCIÓN PARA EL PROGRESSBAR
        hiloProgresBar.start();

        setAppLocale();

    }

    @Override
    public void run() {
        Intent i;
        int cont = 0;

        try {
            while (progressBar.getProgress() != 100) {
                Thread.sleep(200);
                progressBar.incrementProgressBy(cont);
                cont++;
            }
            i = new Intent(this, Login.class);
            startActivity(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAppLocale() {
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
        String idioma = preferencias.getString("idioma", "ESP");
        Resources res = this.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration configuracion = res.getConfiguration();
        configuracion.setLocale(new Locale(idioma.toLowerCase()));
        res.updateConfiguration(configuracion, dm);
    }


}
