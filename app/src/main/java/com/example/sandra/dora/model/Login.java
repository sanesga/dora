package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private Button botonAcceder;
    private EditText editTextCorreo;
    private EditText editTextContrasenya;
    private String correo, contrasenya;
    private FirebaseAuth mAuth;
    private TextView textViewcrearCuenta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //*******************************--REFERENCIAS--********************************************
        editTextCorreo = findViewById(R.id.editTextCorreo);
        editTextContrasenya = findViewById(R.id.editTextContrasenya);
        botonAcceder = findViewById(R.id.botonAcceder);
        textViewcrearCuenta = findViewById(R.id.textViewCrearCuenta);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        botonAcceder.setOnClickListener(this);
        textViewcrearCuenta.setOnClickListener(this);
        //******************************************************************************************


        //instanciamos firebase
        mAuth = FirebaseAuth.getInstance();


    }

    @Override
    public void onClick(View v) {
Intent i =null;
        switch (v.getId()) {
            case (R.id.botonAcceder):
                //obtenemos los datos introducidos por el usuario
               // correo = editTextCorreo.getText().toString();
              correo="prueba@gmail.com";
                contrasenya="123456";
                //contrasenya = editTextContrasenya.getText().toString();

                if(!(correo.equals("") || contrasenya.equals(""))){
                    //llamamos al método que hace login
                    login(correo,contrasenya);
                }else{
                    Toast.makeText(this, "Tiene que rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }
                break;
            case (R.id.textViewCrearCuenta):
                i=new Intent(this, NuevoUsuario.class);
                startActivity(i);
                break;
        }
    }
//cuando vamos atrás, volvemos a cargar la pantalla de bienvenida
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, Bienvenida.class);
        startActivity(i);
    }


    public void login(String correo, String contrasenya){

        mAuth.signInWithEmailAndPassword(correo, contrasenya)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //si login correcto

                            FirebaseUser user = mAuth.getCurrentUser();

                            editTextCorreo.setText(null);
                            editTextContrasenya.setText(null);

                            botonAcceder.setEnabled(false);
                            Intent i = new Intent(Login.this, MenuPrincipal.class);
                            startActivity(i);

                        } else {
                           //si el login incorrecto
                            Toast.makeText(Login.this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        botonAcceder.setEnabled(true);
    }
}
