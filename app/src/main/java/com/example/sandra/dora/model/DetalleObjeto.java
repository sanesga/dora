package com.example.sandra.dora.model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;
import java.io.FileNotFoundException;

public class DetalleObjeto extends AppCompatActivity implements View.OnClickListener {

    private Objeto o;
    private EditText etNameObject, etDescriptionObject, etPlaceObject, etStorageObject;
    private ImageView ivEdit, ivCamera, ivGallery,ivDelete;
    private Button btnSaveChanges;
    private String name, description, place, storage, encoded;
    private ImageView ivDetailObject;
    private Bitmap imageBitmap;
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int REQUEST_RUNTIME_PERMISSION = 1;
    private GalleryPhoto galleryPhoto;
    private Bitmap bitmap;
    private int posicionAlmacenamiento, posicionObjeto;
    private Lugar l;
    private Almacenamiento a;
    private LugarDAO lugarDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_objeto);

        //*******************************--REFERENCIAS--********************************************
        etNameObject = findViewById(R.id.etNameDetailObject);
        etDescriptionObject = findViewById(R.id.tvDescripcionAlimento);
        etStorageObject = findViewById(R.id.etStorageDetailObject);
        ivEdit = findViewById(R.id.ivEdit);  etPlaceObject = findViewById(R.id.etPlaceDetailObject);
        btnSaveChanges = findViewById(R.id.btnSaveChanges);
        ivDetailObject = findViewById(R.id.ivFotoAlimento);
        ivCamera = findViewById(R.id.ivCamera);
        ivGallery = findViewById(R.id.ivGallery);
        ivDelete= findViewById(R.id.botonBorrarObjeto);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        ivEdit.setOnClickListener(this);
        btnSaveChanges.setOnClickListener(this);
        ivDetailObject.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivGallery.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        //******************************************************************************************

        //***************************AÑADIR FOTOS***************************************************
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        //******************************************************************************************

        //recuperamos la posición del almacenamiento seleccionado
        posicionAlmacenamiento= getIntent().getIntExtra("posicionAlmacenamiento",0);

        //recuperamos la posicion del objeto seleccionado
        posicionObjeto=getIntent().getIntExtra("posicionObjeto",0);

        //recuperamos el lugar
        Bundle parametros = this.getIntent().getExtras();
        if (parametros != null) {
            l = (Lugar) getIntent().getExtras().get("lugar");
        }

        //buscamos el objeto seleccionado
        o=l.getListaAlmacenamientos().get(posicionAlmacenamiento).getListaObjetos().get(posicionObjeto);

        //recuperamos el nombre del almacenamiento seleccionado
        a=l.getListaAlmacenamientos().get(posicionAlmacenamiento);

        //ponemos los datos en los edit text
        etNameObject.setText(o.getNombre());
        etDescriptionObject.setText(o.getDescripcion());
        etPlaceObject.setText(l.getNombre());
        System.out.println("nombre del lugar" + l.getNombre());
        System.out.println("nombre del almacenamiento " + a.getNombre());;
        etStorageObject.setText(a.getNombre());

        imageBitmap = o.decodeImage();
        ivDetailObject.setImageBitmap(imageBitmap);

        //desactivamos los editText, hasta que le demos al botón editar
        etNameObject.setEnabled(false);
        etDescriptionObject.setEnabled(false);
        etPlaceObject.setEnabled(false);
        etStorageObject.setEnabled(false);
        ivDetailObject.setEnabled(false);

        //escondemos el boton guardar cambios y los iconos de camara y galeria
        btnSaveChanges.setVisibility(View.INVISIBLE);
        ivGallery.setVisibility(View.GONE);
        ivCamera.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {

        lugarDAO = new LugarDAO(); //hacemos el new para donde lo usemos

        switch (v.getId()) {
            case R.id.ivEdit:
                //activamos los editText, cuando le demos al botón editar
                etNameObject.setEnabled(true);
                etDescriptionObject.setEnabled(true);
                ivDetailObject.setEnabled(true);

                //mostramos el botón guardar cambios
                btnSaveChanges.setVisibility(View.VISIBLE);
                break;

            case R.id.btnSaveChanges:

                //recogemos lo que se ha escrito en los edit text
                name = etNameObject.getText().toString();
                description = etDescriptionObject.getText().toString();
                place = etPlaceObject.getText().toString();
                storage = etStorageObject.getText().toString();

                if(!(name.equals("") && description.equals(""))){
                    //cambiamos los campos
                    o.setNombre(name);
                    o.setDescripcion(description);

                    //codificamos la imagen a base64 (si la hemos cambiado)
                    if (bitmap != null) {
                        encoded = o.encodeImage(bitmap);
                        o.setFoto(encoded);
                    }

                    //guardamos los cambios en la bd
                    lugarDAO.updatePlace(l);

                    Toast.makeText(this, "Cambios guardados", Toast.LENGTH_SHORT).show();

                    etNameObject.setEnabled(false);
                    etDescriptionObject.setEnabled(false);

                }else{
                    Toast.makeText(this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.ivFotoAlimento: //cuando le damos a la foto para cambiarla, lo que se seleccione en el dialogo, se guarda un código en shared preferences

                //hacemos invisible la foto y visibles los iconos de camara y galeria
                ivDetailObject.setVisibility(View.GONE);
                ivCamera.setVisibility(View.VISIBLE);
                ivGallery.setVisibility(View.VISIBLE);

                break;
            case R.id.ivCamera:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST);
                }
                break;
            case R.id.ivGallery:
                if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                    startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                }
                break;

            case R.id.botonBorrarObjeto: //icono borrar objeto

                //mostramos un show dialog para confirmar
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage("¿Está seguro de querer eliminar el objeto?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        cancelar();
                    }
                });
                dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        aceptar();
                    }
                });
                dialog.show();
                break;
        }
    }

        public void aceptar() {
            //borramos el objeto del lugar (es el que estamos utilizando en las pantallas)
            l.getListaAlmacenamientos().get(posicionAlmacenamiento).getListaObjetos().remove(o);

            //lo borramos de la base de datos
            lugarDAO.updatePlace(l);
            //lugarDAO.deleteObject(l, posicionAlmacenamiento, posicionObjeto);
            Toast.makeText(this, "Objeto eliminado", Toast.LENGTH_SHORT).show();

            Intent i= new Intent(this, VerObjetos.class);
            //le pasamos el lugar y la posicion del almacenamiento
            i.putExtra("posicion", posicionAlmacenamiento);
            i.putExtra("lugar", l);
            startActivity(i);

            finish();

        }

        public void cancelar() {
           //no hacemos nada
        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) { //ACCEDEMOS A LA CÁMARA

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            createImage(imageBitmap);

        } else if (requestCode == GALLERY_REQUEST) { //ACCEDEMOS A LA GALERÍA
            // Creamos una ruta en formato Uri para los datos correspondientes a la imagen
            Uri uri = data.getData();

            // Asignamos esa ruta al objeto de la librería
            galleryPhoto.setPhotoUri(uri);

            // Obtenemos la ruta completa para acceder a la imagen
            String photoPath = galleryPhoto.getPath();
            try {
                //para redimensionar la imagen se puede poner detrás del from: .requestSize(512,512)
                Bitmap imageBitmap = ImageLoader.init().from(photoPath).getBitmap();
                createImage(imageBitmap);
            } catch (FileNotFoundException e) {
                Toast.makeText(getApplicationContext(), "Error al elegir la foto", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void createImage(Bitmap bitmap) {
        this.bitmap = bitmap; //asignamos la foto a la variable global

        //visualizamos otra vez la foto y escondemos los iconos
        ivDetailObject.setVisibility(View.VISIBLE);
        ivCamera.setVisibility(View.GONE);
        ivGallery.setVisibility(View.GONE);
        ivDetailObject.setImageBitmap(bitmap);
    }

    //MÉTODO QUE PIDE PERMISOS PARA UTILIZAR LA GALERIA DE FOTOS
    public boolean checkPermissionREAD_EXTERNAL_STORAGE(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialogGaleria("Añadir almacenamiento externo", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_RUNTIME_PERMISSION);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    //SHOWDIALOG PARA PEDIR PERMISO PARA LA GALERIA DE FOTOS
    public void showDialogGaleria(final String msg, final Context context, final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permiso necesario");
        alertBuilder.setMessage(msg + " necesario permiso");
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    //RESULTADO DE OTORGAR O NO LOS PERMISOS
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //no hacemos nada (hay que volver a darle al botón de galeria)
                } else {
                    Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i= new Intent(this, VerObjetos.class);
        //le pasamos el lugar y la posicion del almacenamiento
        i.putExtra("posicion", posicionAlmacenamiento);
        i.putExtra("lugar", l);
        startActivity(i);

        finish();

    }
}
