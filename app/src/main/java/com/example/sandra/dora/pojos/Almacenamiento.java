package com.example.sandra.dora.pojos;

import java.io.Serializable;
import java.util.ArrayList;

public class Almacenamiento implements Serializable {

    private String nombre;
    private ArrayList<Objeto> listaObjetos;

    public Almacenamiento() {
        listaObjetos = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Objeto> getListaObjetos() {
        return listaObjetos;
    }

    public void setListaObjetos(ArrayList<Objeto> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }
}
