package com.example.sandra.dora.model;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class Ubicacion extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;
    private static final int DEFAULT_ZOOM = 15;
    private final LatLng mDefaultLocation = new LatLng(38.986, -0.532944);
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private Double longitud, latitud;
    private Button botonGuardarUbicacion;
    private SharedPreferencesController spc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion);

        //REFERENCIAS PARA LOS MAPAS
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapaCoche);
        mapFragment.getMapAsync(this);

        //*******************************--REFERENCIAS--********************************************
        botonGuardarUbicacion = findViewById(R.id.botonGuardarUbicacion);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        botonGuardarUbicacion.setOnClickListener(this);
        //******************************************************************************************
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocationPermission();
    }

    /**
     * Verifico permisos
     */
    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Retorno de petición de permisos
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    updateLocationUI();
                }
            }
        }
    }

    /**
     * Modificación de la vista para incluir opciones en caso de existir permisos de posicionamiento
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                getDeviceLocation();
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {
        try {
            if (mLocationPermissionGranted) {
                mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            mLastKnownLocation = task.getResult();


                            if (mLastKnownLocation != null) { //si hay una última localización conocida..

                                //recuperamos la longitud y la latitud
                                longitud = mLastKnownLocation.getLongitude();
                                latitud = mLastKnownLocation.getLatitude();

                                //Añadimos marcador
                                mMap.addMarker(new MarkerOptions().title(getString(R.string.ubicacionActual)).position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())));

                                //Movemos la cámara a nuestra posición
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));


                            } else { //si no hay localización conocida..

                                longitud = mDefaultLocation.longitude;
                                latitud = mDefaultLocation.latitude;
                                Toast.makeText(Ubicacion.this, "Asegúrate de tener activa la ubicación", Toast.LENGTH_SHORT).show();
                                //Añadimos marcador
                                mMap.addMarker(new MarkerOptions().title(getString(R.string.ubicacionActual)).position(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)));

                                //Movemos la cámara a nuestra posición
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude), DEFAULT_ZOOM));
                            }
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onClick(View v) { //GUARDAMOS LAS COORDENADAS EN SHARED PREFERENCES CUANDO DAMOS AL BOTÓN GUARDAR

        spc = new SharedPreferencesController(getApplicationContext()); //hacemos el new pasando el contexto

        spc.savePlaceCoordinates(longitud, latitud); //guardamos las coordenadas en sharedpreferences

        // Intent i = new Intent(this, AnyadirLugar.class); //vamos a la pantalla anterior
        //startActivity(i);
        finish(); //FINALIZAMOS ESTA ACTIVIDAD Y PASA A LA ANTERIOR QUE ESTÁ EN ESPERA (AÑADIR LUGAR)
    }
}
