package com.example.sandra.dora.adaptador;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.sandra.dora.pojos.Lugar;
import java.util.ArrayList;


public class MiArrayAdapter<T> extends ArrayAdapter<T> {

    public MiArrayAdapter(Fragment context, ArrayList<T> objects) {
        super(context.getActivity(),0, objects);
    }


    @Override
    public View getView(int position, View convertView,ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listItemView = convertView;

        if(listItemView==null){
            listItemView= inflater.inflate(android.R.layout.simple_list_item_1,parent,false);
        }
        TextView nombre = listItemView.findViewById(android.R.id.text1);

        Lugar l = (Lugar) getItem(position);
        nombre.setText(l.getNombre());
        return listItemView;
    }
}
