package com.example.sandra.dora.pojos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class Objeto implements Serializable {

    private String nombre;
    private String descripcion;
    private String foto;
    private String lugar;
    private String almacenamiento;

    public Objeto() {
    }

    @Override
    public String toString() {
        return "Objeto{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", foto='" + foto + '\'' +
                ", lugar='" + lugar + '\'' +
                ", almacenamiento='" + almacenamiento + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getAlmacenamiento() {
        return almacenamiento;
    }

    public void setAlmacenamiento(String almacenamiento) {
        this.almacenamiento = almacenamiento;
    }

    public Bitmap decodeImage(){ //DECODIFICAMOS LA IMAGEN DE BASE64 A BITMAP
        byte[] decodedString = Base64.decode(this.getFoto(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        //Bitmap bitmap = Bitmap.createScaledBitmap(decodedByte, 130, 200, true); //le damos el tamaño que queremos
        return decodedByte;
    }
    public String encodeImage(Bitmap bitmap){ //CODIFICAMOS LA IMAGEN DE BITMAP A BASE64
        //convertimos el bitmap a string, para poder guardarlo en firebase
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }


}
