package com.example.sandra.dora.controller;

import com.example.sandra.dora.pojos.Alimento;

import java.util.ArrayList;

public interface MyCallBackUnAlimento {
    void onCallBackUnAlimento(Alimento a);
}
