package com.example.sandra.dora.adaptador;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.model.Login;

public class ChangePasswordDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText etNewPassword, etNewPasswordRepeat;
        final TextView tvErrorMessage;

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_change_password_dialog, null);

        //*************************************REFERENCIAS******************************************
        etNewPassword = (EditText) v.findViewById(R.id.etnewpassword);
        etNewPasswordRepeat = (EditText) v.findViewById(R.id.etnewpasswordrepeat);
        tvErrorMessage = (TextView) v.findViewById(R.id.errorMessagePassword);
        //******************************************************************************************

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(v)
                //.setTitle("mi dialogo")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        String password = etNewPassword.getText().toString();
                        String passwordRepeat = etNewPasswordRepeat.getText().toString();

                        if ((!password.equals("")) && (!passwordRepeat.equals(""))) { //SI LAS CONTRASEÑAS NO ESTÁN VACÍAS

                            if (password.equals(passwordRepeat)) { //VERIFICAMOS QUE COINCIDAN

                                if (password.length() >= 6) { //VERIFICAMOS QUE TENGA MÁS DE 6 CARACTERES

                                    //-----------------AMBIAMOS LA CONTRASEÑA-----------------------
                                    UserController dbc = new UserController();
                                    dbc.changePassword(password);
                                    //--------------------------------------------------------------

                                    Toast.makeText(getContext(), R.string.contrasenyaCambiada, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();

                                    Intent i = new Intent(getContext(), Login.class);
                                    startActivity(i);

                                } else {
                                    tvErrorMessage.setText(R.string.errorPatronContrasenya);
                                }
                            } else {
                                tvErrorMessage.setText(R.string.errorConstrasenyaNoCoincide);
                            }
                        } else {
                            tvErrorMessage.setText(R.string.errorContrasenyaVacia);
                        }
                    }
                });
            }
        });
        dialog.show();

        return dialog;
    }
}
