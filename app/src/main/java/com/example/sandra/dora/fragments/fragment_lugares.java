package com.example.sandra.dora.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.adaptador.MiArrayAdapterGridViewLugares;
import com.example.sandra.dora.controller.MyCallBack;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.model.AnyadirLugar;
import com.example.sandra.dora.pojos.Lugar;

import java.util.ArrayList;

public class fragment_lugares extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private LugaresListener listener;
    private GridView gridViewVerLugares;
    private MiArrayAdapterGridViewLugares adapter;
    private Button addPlacesButton;
    private LugarDAO lugarDAO;
    private SharedPreferencesController spc;
    private String id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_fragment_ver_lugares, container, false);
    }

    //esto es un SET de lugares listener, esto hace que se enlace la clase lugares.java con su fragment.
    public void setLugaresListener(LugaresListener listener) {
        this.listener = listener;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //*******************************--REFERENCIAS--********************************************
        gridViewVerLugares = getView().findViewById(R.id.gridViewVerLugares);
        addPlacesButton = getView().findViewById(R.id.addPlaceButton);
        //******************************************************************************************
        gridViewVerLugares.setOnItemClickListener(this);
        addPlacesButton.setOnClickListener(this);
        //******************************************************************************************

        //hacemos el new a lugarDAO para utilizarlo posteriormente
        lugarDAO = new LugarDAO();

        //verificamos en el shared preferences si hay algún lugar para eliminar (no se puede eliminar en la pantalla del mismo lugar, porque no tenemos el arraylist).
        spc = new SharedPreferencesController(getContext());
        id = spc.getPlaceId();

        if (!id.equalsIgnoreCase("")) { //si no está vacío, es que hay algo que eliminar
            lugarDAO.deletePlace(id);
        }
        //reseteamos el id guardado
        spc.resetPlaceId();


        //RECUPERAMOS DATOS DE LA BASE DE DATOS
        lugarDAO.getAll(new MyCallBack() { //hacemos el new del interface para recuperar los datos a través de él
            @Override
            public void onCallBackLugar(ArrayList<Lugar> list) {
                if(list.size()>0){
                    adapter = new MiArrayAdapterGridViewLugares(getActivity(), list); //hacemos el new a miArrayAdapter
                    gridViewVerLugares.setAdapter(adapter); //añadimos el adaptador al gridView
                }else{
                    Toast.makeText(getActivity(), "Cargando base de datos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
            //Obtenemos un lugar que le pasamos a lugaresListener
            listener.onLugarSeleccionado((Lugar) gridViewVerLugares.getAdapter().getItem(position));
        }
    }

    @Override
    public void onClick(View v) { //BOTÓN AÑADIR LUGAR
        Intent i;
        i = new Intent(getContext(), AnyadirLugar.class);
        startActivity(i);
        //getActivity().finish(); //cuando añadimos, cerramos esta pantalla, porque en la siguiente se hace un intent al guardar hacia esta
    }


}
        


