package com.example.sandra.dora.adaptador;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.model.Login;

public class CloseAccountDialog extends DialogFragment {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("¿Está seguro que quiere eliminar la cuenta y todos los datos almacenados?")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            UserController dbc = new UserController();
                            dbc.closeAccount();

                            Toast.makeText(getContext(), "Cuenta eliminada", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(getActivity(), Login.class);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }

}
