package com.example.sandra.dora.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.model.VerLugares;
import com.example.sandra.dora.model.VerObjetos;
import com.example.sandra.dora.model.AnyadirAlmacenamiento;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;

//PANTALLA VISTA DETALLE DEL LUGAR CON MAPA, NOMBRE Y SUS ALMACENAMIENTOS Y AÑADIR ALMACENAMIENTO
public class fragment_almacenamientos extends Fragment implements OnMapReadyCallback, View.OnClickListener, AdapterView.OnItemClickListener {

    //variables mapas
    private GoogleMap mMap;
    private final int DEFAULT_ZOOM = 15;

    private EditText etPlaceName;
    private ListView lvStorageList;
    private ArrayAdapter<String> adapter;
    private Lugar lugarActual; //en toda esta clase, trabajamos con el lugar que se ha seleccionado en la pantalla anterior y que se recibe por el método mostrar detalle
    private  ArrayList<String> storageListName = new ArrayList<>();
    private  Button btnAddStorage, btnSavePlaceChanges;
    private ImageView ivEditPlace, ivDeletePlace;
    private String newNamePlace;
    private LugarDAO lugarDAO;
    private SharedPreferencesController spc;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_fragment_ver_almacenamientos, container, false);
    }

    public void mostrarDetalle(Lugar l) {
        lugarActual = l;
        //*******************************--REFERENCIAS--********************************************
        etPlaceName = getView().findViewById(R.id.etNombreLugar);
        lvStorageList = getView().findViewById(R.id.listViewAlmacenamientos);
        btnAddStorage = getView().findViewById(R.id.btnAddStorage);
        ivEditPlace = getView().findViewById(R.id.ivEditPlace);
        btnSavePlaceChanges= getView().findViewById(R.id.btnSavePlaceChanges);
        ivDeletePlace= getView().findViewById(R.id.ivDeletePlace);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        btnAddStorage.setOnClickListener(this);
        lvStorageList.setOnItemClickListener(this);
        ivEditPlace.setOnClickListener(this);
        btnSavePlaceChanges.setOnClickListener(this);
        ivDeletePlace.setOnClickListener(this);
        //******************************************************************************************

        //desactivamos el edit text del nombre del lugar (hasta que demos al icono de editar)
        etPlaceName.setEnabled(false);
        //ocultamos el botón guardar cambios
        btnSavePlaceChanges.setVisibility(View.GONE);

        //recuperamos los almacenamientos del lugar (se lo hemos pasado al método)
        for (Almacenamiento a:l.getListaAlmacenamientos()) {
            storageListName.add(a.getNombre()); //los pasamos a un array de strings para poder usar el adaptador del sistema
        }

        //creamos el adaptador al listview y se lo asignamos
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, storageListName);
        lvStorageList.setAdapter(adapter);


        //REFERENCIAS PARA LOS MAPAS
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapaLugares);
        mapFragment.getMapAsync(this);


        //PONEMOS EL NOMBRE DEL LUGAR EN EL TEXTVIEW
        etPlaceName.setText(l.getNombre());
    }

        @Override
        public void onMapReady (GoogleMap googleMap){
            mMap = googleMap;
            ubicarLugar();
        }

        public void ubicarLugar () {
            LatLng mDefaultLocation = new LatLng(lugarActual.getLatitud(), lugarActual.getLongitud());

            //Añadimos marcador
            mMap.addMarker(new MarkerOptions().title(getString(R.string.ubicacionActual)).position(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)));

            //Movemos la cámara a nuestra posición
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude), DEFAULT_ZOOM));
        }

        @Override
        public void onClick (View v){

        lugarDAO = new LugarDAO();

        switch (v.getId()){
            case R.id.btnAddStorage: //AÑADIR ALMACENAMIENTO
                Intent i = new Intent(getContext(), AnyadirAlmacenamiento.class);
                i.putExtra("lugar", lugarActual);
                startActivity(i);
                getActivity().finish(); //cerramos esta pantalla, porque cuando vuelva de añadir almacenamiento, se volverá a llamar para que vuelva a cargar la lista de almacenamientos actualizada
                break;
            case R.id.ivEditPlace: //EDITAR LUGAR
                //habilitamos el editText
                etPlaceName.setEnabled(true);
                //mostramos el botón guardar cambios y ocultamos el de añadir lugar
                btnAddStorage.setVisibility(View.GONE);
                btnSavePlaceChanges.setVisibility(View.VISIBLE);
                break;

            case R.id.btnSavePlaceChanges: //BOTÓN GUARDAR CAMBIOS DE LA EDICIÓN

                newNamePlace=etPlaceName.getText().toString(); //recuperamos lo escrito en el editText
                lugarActual.setNombre(newNamePlace); //modificamos el lugar
                lugarDAO.updatePlace(lugarActual); //modificamos en la base de datos
                Toast.makeText(getActivity(), "Cambios guardados", Toast.LENGTH_SHORT).show();
                //deshabilitamos el edit text y ocultamos el botón
                etPlaceName.setEnabled(false);
                btnAddStorage.setVisibility(View.VISIBLE);
                btnSavePlaceChanges.setVisibility(View.GONE);
                break;

            case R.id.ivDeletePlace: //icono de eliminar

                //mostramos un show dialog para confirmar
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
                dialogo1.setMessage("¿Está seguro de querer eliminar el lugar y todo su contenido?");
                dialogo1.setCancelable(false);
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        cancelar();
                    }
                });
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        aceptar();
                    }
                });
                dialogo1.show();
                break;
        }
        }
    public void aceptar() {
        //guardamos en el sharedPreference el id del lugar que queremos eliminar (no lo podemos eliminar aquí porque no tenemos el arraylist de lugares)
        spc = new SharedPreferencesController(getContext());
        spc.savePlaceId(lugarActual.getId());

        Toast.makeText(getActivity(), "Lugar eliminado", Toast.LENGTH_SHORT).show();

        //volvemos a la pantalla anterior
        //enviamos la posición a la que hemos hecho click y el lugar
        Intent i = new Intent(getActivity(), VerLugares.class);
        startActivity(i);

        getActivity().finish();
    }

    public void cancelar() {
        //no hacemos nada
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) { //cuando seleccionamos un almacenamiento, vamos a la pantalla de almacenamientos (donde veremos los objetos que contienen)

        //enviamos la posición a la que hemos hecho click y el lugar
        Intent i = new Intent(getActivity(), VerObjetos.class);
        i.putExtra("posicion",position);
        i.putExtra("lugar", lugarActual);

        startActivity(i);
    }


}


