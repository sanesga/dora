package com.example.sandra.dora.adaptador;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.sandra.dora.R;
import com.example.sandra.dora.pojos.Lugar;
import java.util.ArrayList;

    public class MiArrayAdapterGridViewLugares<T> extends ArrayAdapter<T> {


        public MiArrayAdapterGridViewLugares(FragmentActivity context, ArrayList<T> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View listItemView = convertView;

            if (listItemView == null) {
                listItemView = inflater.inflate(R.layout.layout_gridview_ver_lugares, parent, false);
            }
            ImageView image = listItemView.findViewById(R.id.ivObject);
            TextView placeName = listItemView.findViewById(R.id.text);

            //obtenemos el lugar seleccionado
            Lugar l = (Lugar)getItem(position);
            //seteamos el nombre en el textview
            placeName.setText(l.getNombre());

            return listItemView;
        }
    }

