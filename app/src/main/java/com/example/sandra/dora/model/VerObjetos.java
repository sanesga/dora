package com.example.sandra.dora.model;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.adaptador.MiArrayAdapterGridViewObjetos;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;

import java.util.ArrayList;

public class VerObjetos extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private GridView gvObjects;
    private Button btnAddObject, btnSaveStorageChanges;
    private EditText etSelectedStorageName;
    private MiArrayAdapterGridViewObjetos<Objeto> adapter;
    private ArrayList<Objeto> listaObjetos;
    private Lugar l;
    private int posicion;
    private Almacenamiento a;
    private ImageView ivDeleteStorage, ivEditStorage;
    private LugarDAO lugarDAO;
    private String storageNewName;
    private Toolbar toolbar;
    private UserController uc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_objetos);

        //*******************************--REFERENCIAS--********************************************
        gvObjects = findViewById(R.id.gvObjects);
        btnAddObject = findViewById(R.id.btnAddObject);
        etSelectedStorageName = findViewById(R.id.etSelectedStorageName);
        ivDeleteStorage = findViewById(R.id.ivDeleteStorage);
        ivEditStorage = findViewById(R.id.ivEditStorage);
        btnSaveStorageChanges = findViewById(R.id.btnSaveStorageChanges);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        btnAddObject.setOnClickListener(this);
        gvObjects.setOnItemClickListener(this);
        ivEditStorage.setOnClickListener(this);
        ivDeleteStorage.setOnClickListener(this);
        btnSaveStorageChanges.setOnClickListener(this);
        //******************************************************************************************

        //----------TOOLBAR----------
        setSupportActionBar(toolbar);
        //--------------------------

        //ocultamos el botón guardar cambios en el almacenamiento
        btnSaveStorageChanges.setVisibility(View.GONE);

        //recuperamos la posición del almacenamiento seleccionado
        posicion = getIntent().getIntExtra("posicion", 0);

        //a partir de la posicion enviada, recuperamos el lugar
        Bundle parametros = this.getIntent().getExtras();
        if (parametros != null) {
            l = (Lugar) getIntent().getExtras().get("lugar");
        }

        //almacenamiento seleccionado
        a = l.getListaAlmacenamientos().get(posicion);

        //desactivamos el edit a no ser que presionemos el icono editar
        etSelectedStorageName.setEnabled(false);
        etSelectedStorageName.setText(a.getNombre()); //nombre del almacenamiento seleccionado

        //lista de objetos de ese almacenamiento
        listaObjetos = new ArrayList<>();
        listaObjetos = a.getListaObjetos();

        //creamos el adaptador
        adapter = new MiArrayAdapterGridViewObjetos(getApplicationContext(), listaObjetos);
        gvObjects.setAdapter(adapter);
    }

    public void actionMenu(View view) {

        Intent i = null;

        switch (view.getTag().toString()) {
            case "ajustes":
                i = new Intent(this, Configuracion.class);
                break;
            case "user":
                //aqui llamaremos al metodo getUser y obtendremos el nombre del usuario
                uc = new UserController();
                String userMail;
                userMail = uc.getUserData();
                i = new Intent(this, Account.class);
                i.putExtra("mailAccount", userMail);
                break;
            case "home":
                i = new Intent(this, MenuPrincipal.class);
                break;
        }
        startActivity(i);
    }


    @Override
    public void onClick(View v) {

        lugarDAO = new LugarDAO();

        switch (v.getId()) {
            case R.id.btnAddObject: //AÑADIR OBJETO
                Intent i = new Intent(this, AnyadirObjeto.class);
                //le pasamos el lugar y la posicion del almacenamiento
                i.putExtra("lugar", l);
                i.putExtra("posicion", posicion);
                startActivity(i);
                finish();
                break;

            case R.id.ivEditStorage: //icono editar

                //mostramos botón guardar cambios y escondemos el de añadir almacenamiento
                btnAddObject.setVisibility(View.GONE);
                btnSaveStorageChanges.setVisibility(View.VISIBLE);

                //habilitamos el editText
                etSelectedStorageName.setEnabled(true);


                break;

            case R.id.btnSaveStorageChanges: //cuando le damos al botón guardar cambios (nombre del almacenamiento

                //recogemos el cambio de nombre
                storageNewName = etSelectedStorageName.getText().toString();

                //lo cambiamos en el objeto
                a.setNombre(storageNewName);

                //actualizamos el lugar en la base de datos (con el nombre del almacenamiento cambiado)
                lugarDAO.updatePlace(l);

                Toast.makeText(this, "Cambios guardados", Toast.LENGTH_SHORT).show();

                //volvemos a dejar como antes
                etSelectedStorageName.setEnabled(false);
                btnSaveStorageChanges.setVisibility(View.GONE);
                btnAddObject.setVisibility(View.VISIBLE);

                break;
            case R.id.ivDeleteStorage: //icono borrar
                //mostramos un show dialog para confirmar
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                dialogo1.setMessage("¿Está seguro de querer eliminar el almacenamiento y todo su contenido?");
                dialogo1.setCancelable(false);
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        cancelar();
                    }
                });
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        aceptar();
                    }
                });
                dialogo1.show();
                break;
        }
    }

    public void aceptar() {

        //borramos el almacenamiento del lugar
        l.getListaAlmacenamientos().remove(posicion);

        lugarDAO.updatePlace(l);
        Toast.makeText(this, "Almacenamiento eliminado", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(this, VerAlmacenamientos.class);
        //le pasamos el lugar y la posicion del almacenamiento
        i.putExtra("lugar", l);
        startActivity(i);

        finish();
    }

    public void cancelar() {
        //no hacemos nada
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) { //HACEMOS CLICK EN UN OBJETO DEL GRIDVIEW

        Intent i = new Intent(this, DetalleObjeto.class);
        i.putExtra("posicionAlmacenamiento", posicion);
        i.putExtra("lugar", l);
        i.putExtra("posicionObjeto", position);
        startActivity(i);

        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, VerAlmacenamientos.class);
        i.putExtra("lugar", l);
        startActivity(i);
    }
}
