package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.MyCallBack;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Lugar;

import java.util.ArrayList;

public class ElegirLugarAlmacenamiento extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner spinnerPlace, spinnerStorage;
    private ArrayList<String> listPlacesNames = new ArrayList<>();
    private ArrayList<String> listStoragesNames = new ArrayList<>();
    private ArrayAdapter<String> adapterPlaces, adapterStorages;
    private Button btnSaveSelection;
    private String placeName, storageName;
    private SharedPreferencesController spc;
    private LugarDAO lugarDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_lugar_almacenamiento);

        //*******************************--REFERENCIAS--********************************************
        spinnerPlace = findViewById(R.id.spinnerChoosePlace);
        spinnerStorage = findViewById(R.id.spinnerChooseStorage);
        btnSaveSelection = findViewById(R.id.btnSaveSelection);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        btnSaveSelection.setOnClickListener(this);
        spinnerPlace.setOnItemSelectedListener(this);
        //******************************************************************************************

        //recuperamos los lugares y los almacenamientos de la base de datos
        lugarDAO = new LugarDAO();
        lugarDAO.getAll(new MyCallBack() {
            @Override
            public void onCallBackLugar(ArrayList<Lugar> list) { //OBTENEMOS LA LISTA DE LUGARES
                for (Lugar l : list) {
                    listPlacesNames.add(l.getNombre()); //CREAMOS UN ARRAYLIST DE STRING PARA PONERLO EN EL ADAPTER DEL SPINNER
                }
                adapterPlaces = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, listPlacesNames);
                spinnerPlace.setAdapter(adapterPlaces);
            }
        });
    }
    @Override
    public void onClick(View v) { //BOTÓN GUARDAR SELECCIÓN DE LUGAR Y ALMACENAMIENTO Y PASAR A LA PANTALLA DE AÑADIR OBJETO
        //guardamos los nombres en shared preferences
        spc = new SharedPreferencesController(getApplicationContext());
        spc.savePlaceName(placeName);
        spc.saveNameStorage(storageName);
        //vamos a la pantalla añadir objeto
        Intent i= new Intent(this, AnyadirObjeto.class);
        startActivity(i);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { //CUANDO SE SELECCIONA UN LUGAR DEL SPINNER LUGARES
        listStoragesNames.clear();
        placeName = spinnerPlace.getSelectedItem().toString(); //OBTENEMOS EL VALOR SELECCIONADO EN EL SPINNER
        //llamamos a la base de datos pasándole el lugar seleccionado para rellenar el siguiente spinner con todos los almacenamientos
        //almacenamientoDAO = new AlmacenamientoDAO();
        //almacenamientoDAO.getAll(placeName, new MyCallBack() {
          /*  @Override
            public void onCallBackLugar(ArrayList<Lugar> list) {
                //este no lo usamos
            }
            @Override
            public void onCallBackAlmacenamiento(ArrayList<Almacenamiento> list) { //OBTENEMOS UN ARRAYLIST DE ALMACENAMIENTOS
                for (Almacenamiento a : list) {
                    listStoragesNames.add(a.getNombre());
                }
                adapterStorages = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, listStoragesNames);
                spinnerStorage.setAdapter(adapterStorages);

                //recuperamos el almacenamiento elegido
                storageName = spinnerStorage.getSelectedItem().toString();
            }
            @Override
            public void onCallBackObjeto(ArrayList<Objeto> list) {
                //este no lo usamos
            }
        });*/
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) { //CUANDO NO SE SELECCIONA NADA
        //no hacemos nada
    }
}
