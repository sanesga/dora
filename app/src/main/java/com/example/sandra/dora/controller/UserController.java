package com.example.sandra.dora.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserController {

    private DatabaseReference referencia;
    private FirebaseUser user;
    private String userId;

    public UserController() {
        user = FirebaseAuth.getInstance().getCurrentUser(); //obtenemos el usuario logeado
        referencia = FirebaseDatabase.getInstance().getReference(); //otenemos la base de datos que estamos utilizando
        userId = user.getUid(); //obtenemos el id del usuario logeado para guardar los datos en su bd

    }

    public String getUserData() {

        String email = "";

        if (user != null) {
            // Name, email address, and profile photo Url
            // String name = user.getDisplayName();
            email = user.getEmail();
            // Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            //boolean emailVerified = user.isEmailVerified();

            /*if (emailVerified) {
                resultado=name;
            } else {
                resultado="Email no validado";
            }*/

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
        }
        return email;
    }

    public void changeEmail(String newEmail) {
        user.updateEmail(newEmail);
    }

    public void signOut() { //cierra la sesión
        FirebaseAuth.getInstance().signOut();
    }

    public void changePassword(String newPassword) {
        user.updatePassword(newPassword);
    }

    public void closeAccount() {
        user.delete();
    }

}


