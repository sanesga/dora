package com.example.sandra.dora.model;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.SharedPreferencesController;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class VerCoche extends AppCompatActivity implements OnMapReadyCallback {

    //variables mapas
    GoogleMap mMap;
    final int DEFAULT_ZOOM = 15;
    private double longitud, latitud;
    private SharedPreferencesController spc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_coche);

        //REFERENCIAS PARA LOS MAPAS
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapaVerCoche);
        mapFragment.getMapAsync(this);

        //RECUPERAMOS LOS DATOS DE SHARED PREFERENCES (LA PRIMERA VEZ ESTARÁ VACÍO)
        spc = new SharedPreferencesController(getApplication());
        longitud = (double) spc.getCarLongitude();
        latitud = (double) spc.getCarLatitude();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //TIPO DE MAPA
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        if (!(latitud == 0 || longitud == 0)) {
            ubicarLugar();
        } else {
            Toast.makeText(this, "No se ha estacionado ningún vehículo", Toast.LENGTH_SHORT).show();
        }
    }

    public void ubicarLugar() {

        LatLng mDefaultLocation = new LatLng(latitud, longitud);

        //Añadimos marcador
        mMap.addMarker(new MarkerOptions().title(getString(R.string.ubicacionActual)).position(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)));

        //Movemos la cámara a nuestra posición
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude), DEFAULT_ZOOM));
    }
}
