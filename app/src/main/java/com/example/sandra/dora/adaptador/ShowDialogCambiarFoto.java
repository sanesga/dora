package com.example.sandra.dora.adaptador;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.SharedPreferencesController;

public class ShowDialogCambiarFoto extends DialogFragment {

    private SharedPreferencesController spc;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //creamos nuestra propia vista**********************
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.layout_elegir_imagen, null);

        builder.setView(v);
        //*******************************************************

        ImageView ivGallery = v.findViewById(R.id.ivGallery);
        ImageView ivCamera = v.findViewById(R.id.ivCamera);

        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //hacemos click en galería

                spc = new SharedPreferencesController(getContext());
                spc.saveGallerySignal();
                dismiss();
            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //hacemos click en cámara
                spc = new SharedPreferencesController(getContext());
                spc.saveCameraSignal();
                dismiss();

            }
        });

        return builder.create();
    }
}

