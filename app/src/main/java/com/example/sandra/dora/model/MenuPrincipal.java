package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.dao.LugarDAO;


public class MenuPrincipal extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageButton imageButtonAnyadirCoche, imageButtonVerObjetos, imageButtonVerLugares, imageButtonVerCoche;
    private UserController uc;
    private ImageView ivNevera;
    private LugarDAO dbc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        //*******************************--REFERENCIAS--********************************************
        toolbar = findViewById(R.id.toolbar);
        imageButtonAnyadirCoche = findViewById(R.id.imageButtonAnyadirCoche);
        imageButtonVerLugares = findViewById(R.id.imageButtonVerLugares);
        imageButtonVerCoche = findViewById(R.id.imageButtonVerCoche);
        imageButtonVerObjetos = findViewById(R.id.imageButtonVerObjetos);
        ivNevera = findViewById(R.id.ivNevera);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        imageButtonAnyadirCoche.setOnClickListener(this);
        imageButtonVerLugares.setOnClickListener(this);
        imageButtonVerCoche.setOnClickListener(this);
        imageButtonVerObjetos.setOnClickListener(this);
        ivNevera.setOnClickListener(this);
        //******************************************************************************************

        //----------TOOLBAR----------
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //--------------------------

    }

    public void actionMenu(View view) {

        Intent i = null;

        switch (view.getTag().toString()) {
            case "ajustes":
                i = new Intent(this, Configuracion.class);
                startActivity(i);
                break;
            case "user":
                //aqui llamaremos al metodo getUser y obtendremos el nombre del usuario
                uc = new UserController();
                String userMail;
                userMail = uc.getUserData();

                i = new Intent(this, Account.class);
                i.putExtra("mailAccount", userMail);
                startActivity(i);
                break;
            case "home":
                // i = new Intent(this, MenuPrincipal.class);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        Intent i = null;
        switch (v.getId()) {
            case R.id.imageButtonAnyadirCoche:
                i = new Intent(this, AnyadirCoche.class);
                break;
            case R.id.imageButtonVerLugares:
                i = new Intent(this, VerLugares.class);
                break;

            case R.id.imageButtonVerCoche:
                i = new Intent(this, VerCoche.class);
                break;
            case R.id.imageButtonVerObjetos:
                i = new Intent(this, SearchObject.class);
                break;
            case R.id.ivNevera:
                i = new Intent(this, Nevera.class);
                break;
        }
        startActivity(i);
    }
}
