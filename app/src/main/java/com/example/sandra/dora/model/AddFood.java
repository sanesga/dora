package com.example.sandra.dora.model;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.example.sandra.dora.dao.AlimentoDAO;
import com.example.sandra.dora.pojos.Alimento;

public class AddFood extends AppCompatActivity implements View.OnClickListener {

    private TextView tvAddPicture;
    private static final int CAMERA_REQUEST = 1;
    private Bitmap bitmap;
    private ImageView ivShowPicture;
    private EditText etNameFood, etDescriptionFood, etPriceFood;
    private Button btnSaveFood;
    private String nameFood, descriptionFood, imageFood, codebar,price;
    private double priceFood;
    private Alimento a;
    private AlimentoDAO alimentoDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);


        //*******************************--REFERENCIAS--********************************************
        tvAddPicture = findViewById(R.id.tvAddPicture);
        ivShowPicture = findViewById(R.id.ivShowPicture);
        etNameFood = findViewById(R.id.etNameFood);
        etDescriptionFood = findViewById(R.id.etDescriptionFood);
        etPriceFood = findViewById(R.id.etPriceFood);
        btnSaveFood = findViewById(R.id.btnSaveFood);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        tvAddPicture.setOnClickListener(this);
        btnSaveFood.setOnClickListener(this);
        //******************************************************************************************

        //recogemos el codebar
        codebar = getIntent().getStringExtra("codebar");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvAddPicture: //le damos al botón añadir foto del alimento ABRIMOS LA CÁMARA
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST);
                }
                break;
            case R.id.btnSaveFood: //le damos al botón guardar alimento
                nameFood = etNameFood.getText().toString();
                descriptionFood = etDescriptionFood.getText().toString();
                price = etPriceFood.getText().toString();

                if (!(nameFood.equals("") && descriptionFood.equals("") && price.equals("")) && this.bitmap != null) {
                    a = new Alimento();
                    a.setNombre(nameFood);
                    a.setDescripcion(descriptionFood);

                    priceFood = Double.parseDouble(price);
                    a.setPrecio(priceFood);

                    a.setCodigoBarras(codebar);

                    //pasamos la imagen de bitmap a base64 para guardarla en la bd
                    imageFood = a.encodeImage(this.bitmap);

                    a.setFoto(imageFood);

                    alimentoDAO = new AlimentoDAO();
                    alimentoDAO.add(a);


                    //añadimos el alimento a la lista de alimentos actual (es la lista que recuperamos en la vista anterior, la de alimentos que queremos comprar)
                    alimentoDAO.addCurrentList(a);

                    Toast.makeText(this, "Alimento añadido a la base de datos", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(this, MenuPrincipal.class); //al guardar volvemos a la lista de productos
                    startActivity(i);
                    finish();

                } else {
                    Toast.makeText(this, "Debe rellenar todos los campos y capturar una imagen", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void createImage(Bitmap bitmap) {
        this.bitmap = bitmap; //asignamos la foto a la variable global
        ivShowPicture.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) { //ACCEDEMOS A LA CÁMARA
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            createImage(imageBitmap);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, Nevera.class); //si no queremos añadir el alimento, volvemos a la lista
        startActivity(i);
        finish();
    }
}
