package com.example.sandra.dora.model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandra.dora.R;
import com.example.sandra.dora.dao.LugarDAO;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;
import java.io.FileNotFoundException;

public class AnyadirObjeto extends AppCompatActivity implements View.OnClickListener {

    private TextView textViewAnyadirFoto;
    private RadioGroup radiogroup;
    private ImageView imageViewFoto;
    private GalleryPhoto galleryPhoto;
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    private RadioButton radioButtonGaleria;
    private RadioButton radioButtonCamara;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int REQUEST_RUNTIME_PERMISSION = 1;
    private Button botonGuardarObjeto;
    private EditText etObjectName, etObjectDescription;
    private Objeto o;
    private String nombre, encoded, descripcion;
    private Bitmap bitmap;
    private Lugar l;
    private int posicion;
    private Almacenamiento a;
    private LugarDAO lugarDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_objeto);

        //*******************************--REFERENCIAS--********************************************
        textViewAnyadirFoto = findViewById(R.id.tvAddPicture);
        radiogroup = findViewById(R.id.radioGroup);
        imageViewFoto = findViewById(R.id.ivShowPicture);
        radioButtonCamara = findViewById(R.id.rbCamera);
        radioButtonGaleria = findViewById(R.id.rbGallery);
        botonGuardarObjeto = findViewById(R.id.btnSaveFood);
        etObjectDescription = findViewById(R.id.etDescriptionFood);
        etObjectName = findViewById(R.id.etNameFood);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        textViewAnyadirFoto.setOnClickListener(this);
        radioButtonGaleria.setOnClickListener(this);
        radioButtonCamara.setOnClickListener(this);
        botonGuardarObjeto.setOnClickListener(this);
        //******************************************************************************************

        //***************************AÑADIR FOTOS***************************************************
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        //******************************************************************************************


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvAddPicture:
                radiogroup.setVisibility(View.VISIBLE);
                break;
            case R.id.rbCamera: //ABRIMOS LA CÁMARA

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST);
                }
                break;
            case R.id.rbGallery: //ABRIMOS LA GALERÍA
                if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                    startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                }
                break;
            case R.id.btnSaveFood: //GUARDAMOS EL OBJETO EN LA BASE DE DATOS

                //recuperamos el lugar donde se guardará el objeto y la posición del almacenamiento donde va
                posicion= getIntent().getIntExtra("posicion",0); //recuperamos la posición del almacenamiento seleccionado
                //recuperamos el lugar
                Bundle parametros = this.getIntent().getExtras();
                if (parametros != null) {
                    l = (Lugar) getIntent().getExtras().get("lugar");
                }
                //a partir de la posicion enviada, recuperamos el almacenamiento
                a=l.getListaAlmacenamientos().get(posicion);

                //recuperamos los datos para crear el objeto
                nombre = etObjectName.getText().toString(); //nombre
                descripcion=etObjectDescription.getText().toString();

                if(!(nombre.equals("") && descripcion.equals("")) && bitmap!=null ){

                    o = new Objeto();//creamos un objeto nuevo
                    o.setNombre(nombre);
                    o.setDescripcion(descripcion);

                    //convertimos el bitmap a string, para poder guardarlo en firebase
                    encoded = o.encodeImage(bitmap);
                    o.setFoto(encoded);

                    //seteamos las claves ajenas lugar y almacenamiento
                    o.setLugar(l.getNombre());
                    o.setAlmacenamiento(a.getNombre());

                    //añadimos el objeto al almacenamiento y al lugar
                    a.getListaObjetos().add(o);

                    //guardamos el objeto lugar actualizado en la base de datos
                    lugarDAO = new LugarDAO();
                    lugarDAO.updatePlace(l);

                    Toast.makeText(this, "Objeto guardado", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(this, VerObjetos.class);//Guardamos el objeto y volvemos a cargar la pantalla anterior
                    i.putExtra("lugar" , l);
                    i.putExtra("posicion", posicion);
                    startActivity(i);

                    finish();

                }else{
                    Toast.makeText(this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void createImage(Bitmap bitmap) {
        this.bitmap = bitmap; //asignamos la foto a la variable global
        imageViewFoto.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) { //ACCEDEMOS A LA CÁMARA

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            createImage(imageBitmap);

        } else if (requestCode == GALLERY_REQUEST) { //ACCEDEMOS A LA GALERÍA
            // Creamos una ruta en formato Uri para los datos correspondientes a la imagen

            if(data!=null){

                Uri uri = data.getData();

                // Asignamos esa ruta al objeto de la librería
                galleryPhoto.setPhotoUri(uri);

                // Obtenemos la ruta completa para acceder a la imagen
                String photoPath = galleryPhoto.getPath();
                try {
                    //para redimensionar la imagen se puede poner detrás del from: .requestSize(512,512)
                    Bitmap imageBitmap = ImageLoader.init().from(photoPath).getBitmap();
                    createImage(imageBitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Error al elegir la foto", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    //MÉTODO QUE PIDE PERMISOS PARA UTILIZAR LA GALERIA DE FOTOS
    public boolean checkPermissionREAD_EXTERNAL_STORAGE(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialogGaleria("Añadir almacenamiento externo", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_RUNTIME_PERMISSION);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    //SHOWDIALOG PARA PEDIR PERMISO PARA LA GALERIA DE FOTOS
    public void showDialogGaleria(final String msg, final Context context, final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permiso necesario");
        alertBuilder.setMessage(msg + " necesario permiso");
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    //RESULTADO DE OTORGAR O NO LOS PERMISOS
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //no hacemos nada (hay que volver a darle al botón de galeria)
                } else {
                    Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
