package com.example.sandra.dora.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sandra.dora.R;
import com.example.sandra.dora.adaptador.ChangeMailDialog;
import com.example.sandra.dora.adaptador.ChangePasswordDialog;
import com.example.sandra.dora.adaptador.CloseAccountDialog;
import com.example.sandra.dora.controller.UserController;

public class Account extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private TextView tv;
    private ListView lv;
    private ArrayAdapter<String> adapter;
    private String[] items;
    private UserController dbc;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        //*******************************--REFERENCIAS--********************************************
        tv = findViewById(R.id.tvmail);
        lv = findViewById(R.id.lvconfigurationaccount);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************

        //******************************************************************************************

        //-------------------------TEXTVIEW-----------------------------------------------
        //RECOGEMOS EL MAIL DEL USUARIO LOGEADO
        String mail = getIntent().getStringExtra("mailAccount");
        //LO ESCRIBIMOS EN EL TEXTVIEW DE LA PANTALLA DE CONFIGURACION
        tv.setText(mail);
        //---------------------------------------------------------------------------------


        //------------------------LISTVIEW-------------------------------------------------
        items = new String[]{getString(R.string.cerrarSesion), getString(R.string.modificarCorreo), getString(R.string.modificarContraseña), getString(R.string.eliminarCuenta)};
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        lv.setAdapter(adapter);

        //COGEMOS LA POSICIÓN SELECCIONADA
        lv.setOnItemClickListener(this);
        //---------------------------------------------------------------------------------

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        dbc = new UserController();
        FragmentManager fm;
        switch (position) {
            case 0: //CERRAR SESIÓN
                dbc.signOut();
                Intent i = new Intent(this, Login.class);
                startActivity(i);
                break;
            case 1: //MODIFICAR CORREO
                fm = getSupportFragmentManager();
                ChangeMailDialog mailDialog = new ChangeMailDialog();
                mailDialog.show(fm, "dialog");
                break;
            case 2: //MODIFICAR CONTRASEÑA
                fm = getSupportFragmentManager();
                ChangePasswordDialog passDialog = new ChangePasswordDialog();
                passDialog.show(fm, "dialog");
                break;
            case 3: //ELIMINAR CUENTA
                fm = getSupportFragmentManager();
                CloseAccountDialog closeAccountDialog = new CloseAccountDialog();
                closeAccountDialog.show(fm, "dialog");
                break;
        }
    }
}
