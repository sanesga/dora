package com.example.sandra.dora.dao;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.sandra.dora.controller.MyCallBack;
import com.example.sandra.dora.controller.MyCallBackAlimentos;
import com.example.sandra.dora.pojos.Alimento;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LugarDAO {

    private DatabaseReference referencia;
    private FirebaseUser user;
    private String userId;
    private ArrayList<Lugar> placesList;
    private Lugar l;
    ArrayList<Almacenamiento> listaAlmacenamientos;
    ArrayList<Alimento> listaAlimentos;
    private Alimento a = null;

    public LugarDAO() {

        user = FirebaseAuth.getInstance().getCurrentUser(); //obtenemos el usuario logeado
        referencia = FirebaseDatabase.getInstance().getReference(); //otenemos la base de datos que estamos utilizando
        userId = user.getUid(); //obtenemos el id del usuario logeado para guardar los datos en su bd

        placesList = new ArrayList<>();
        listaAlimentos = new ArrayList<>();
    }


    public void add(Lugar l) {
        //guardamos el objeto lugar en usuarios, lugares (a cada lugar le da un id único)
        referencia.child("Usuarios").child(userId).child("Lugares").push().setValue(l);

        //de esta forma asignamos como clave primaria el nombre del lugar para facilitar la búsqueda más tarde
        //referencia.child("Usuarios").child(userId).child("Lugares").child(l.getNombre()).setValue(l);

    }

    public void updatePlace(Lugar l) {
        referencia.child("Usuarios").child(userId).child("Lugares").child(l.getId()).setValue(l);
    }

    public void getAll(final MyCallBack myCallBack) {

        //referencia es la instancia a nuestra base de datos
        DatabaseReference userRef = referencia.child("Usuarios").child(userId);

        userRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot etiquetaLugares = dataSnapshot.child("Lugares");//etiqueta lugares de cada usuario logeado

                for (DataSnapshot lugar : etiquetaLugares.getChildren()) { //cada lugar dentro de lugares

                    l = new Lugar(); //creamos un lugar

                    //guardamos el id
                    l.setId(lugar.getKey());

                    //obtenemos el valor de los atributos de lugar, especificando su nombre, por si añadimos más en el futuro
                    l.setNombre((String) lugar.child("nombre").getValue());

                    //LOS NÚMEROS, AUNQUE LOS GUARDAMOS CON EL OBJECTO COMO DOUBLES, FIREBASE NOS LOS DEVUELVE COMO OBJECT. lOS PASAMOS A STRING Y DE AHÍ A DOUBLE PARA GOOGLE MAPS (SOLO ACEPTA DOUBLES)
                    String latitudString = lugar.child("latitud").getValue().toString();
                    String longitudString = lugar.child("longitud").getValue().toString();

                    l.setLatitud(Double.parseDouble(latitudString));
                    l.setLongitud(Double.parseDouble(longitudString));

                    if (lugar.hasChild("listaAlmacenamientos")) { //si hay almacenamientos, leerlos
                        listaAlmacenamientos = leerAlmacenamiento(lugar); //leemos los almacenamientos
                        l.setListaAlmacenamientos(listaAlmacenamientos); //añadimos los almacenamientos al lugar
                    }

                    placesList.add(l); //añadimos los datos a la lista de lugares

                }
                //usamos un interface para guardar los datos a través de él, ya que la llamada a firebase es asíncrona y no podemos devolver los datos a través de este método, porque se intentarán enviar antes de que estén listos.
                myCallBack.onCallBackLugar(placesList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { //fallo al leer la base de datos
                Log.d("Error base de datos","Error al leer datos de la base de datos");
            }
        });
    }

    public ArrayList<Almacenamiento> leerAlmacenamiento(DataSnapshot lugar) {

        Almacenamiento a = null;
        Objeto o;
        ArrayList<Almacenamiento> listaAlmacenamientos = new ArrayList<>();
        ArrayList<Objeto> listaObjetos;


        for (DataSnapshot almacenamiento : lugar.child("listaAlmacenamientos").getChildren()) {

            a = new Almacenamiento();
            a.setNombre(almacenamiento.child("nombre").getValue().toString());


            if (almacenamiento.hasChild("listaObjetos")) { //SI HAY OBJETOS, LOS LEEMOS
                listaObjetos = leerObjetos(almacenamiento, lugar); //recuperamos los objetos de este almacenamiento
                a.setListaObjetos(listaObjetos); //añadimos el arraylist de objetos al almacenamiento
            }

            listaAlmacenamientos.add(a); //añadimos el almacenamiento al arraylist de almacenamientos
        }
        return listaAlmacenamientos;
    }

    public ArrayList<Objeto> leerObjetos(DataSnapshot almacenamiento, DataSnapshot lugar) {

        Objeto o = null;
        ArrayList<Objeto> listaObjetos = new ArrayList<>();

        for (DataSnapshot objeto : almacenamiento.child("listaObjetos").getChildren()) {
            o = new Objeto();
            o.setDescripcion(objeto.child("descripcion").getValue().toString());
            o.setNombre(objeto.child("nombre").getValue().toString());
            o.setFoto(objeto.child("foto").getValue().toString());
            o.setLugar(objeto.child("lugar").getValue().toString());
            o.setAlmacenamiento(objeto.child("almacenamiento").getValue().toString());

            listaObjetos.add(o); //añadimos los objetos leídos al arraylist de objetos

        }
        return listaObjetos;
    }

    public void deletePlace(String id) {
        referencia.child("Usuarios").child(userId).child("Lugares").child(id).removeValue();
    }
}
