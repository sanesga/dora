package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.MyCallBackAlimentos;
import com.example.sandra.dora.dao.AlimentoDAO;
import com.example.sandra.dora.pojos.Alimento;

import java.util.ArrayList;

public class Nevera extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBarCode;
    private ListView lvFood;
    private AlimentoDAO alimentoDAO;
    private ArrayList<String> listaAlimentosActuales = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private TextView tvTotal;
    private double costeTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nevera);

        //*******************************--REFERENCIAS--********************************************
        ivBarCode = findViewById(R.id.botonScanner);
        lvFood = findViewById(R.id.lvFood);
        tvTotal=findViewById(R.id.tvTotal);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        ivBarCode.setOnClickListener(this);
        //******************************************************************************************

        //recuperamos los alimentos que haya en la lista alimentos actuales (los que queremos comprar)
        alimentoDAO = new AlimentoDAO();

        //limpiamos la lista antes de cargarla
        listaAlimentosActuales.clear();

        alimentoDAO.getCurrentFood(new MyCallBackAlimentos() {
            @Override
            public void onCallBackAlimentos(ArrayList<Alimento> list) { //nos devuelve todos los alimentos de la lista actual
                for (Alimento a : list) {
                    listaAlimentosActuales.add(a.getNombre());
                    //Cargamos el listview con los alimentos recuperados
                    adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, listaAlimentosActuales);
                    lvFood.setAdapter(adapter);

                    enviarListaAlimentos(list);
                    calcularTotal(list);  //seteamos el total de los alimentos de la lista en su textview
                }
            }
        });
    }

    private void calcularTotal(ArrayList<Alimento> list) {
        costeTotal=0;
        for (Alimento a:list) {
            costeTotal+=a.getPrecio();
        }
        tvTotal.setText(String.valueOf(costeTotal));
    }



    @Override
    public void onClick(View v) { //AÑADIR ALIMENTO

        switch (v.getId()) {
            case R.id.botonScanner:
                Intent i = new Intent(this, Scan.class);
                startActivity(i);
                finish();
                break;
        }
    }

    public void enviarListaAlimentos(final ArrayList<Alimento> list) { //pasamos la lista de alimentos a la siguiente pantalla a través del intent (lo hacemos así porque no se puede usar la lista fuera del mycallback)
        lvFood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DetalleAlimento.class);
                Alimento alimentoActual = list.get(position);
                System.out.println("alimento que pasamos " + alimentoActual);
                i.putExtra("alimento", alimentoActual);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

