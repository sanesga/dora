package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sandra.dora.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

public class NuevoUsuario extends AppCompatActivity implements View.OnClickListener {

    private Button botonCrearUsuario;
    private EditText editTextCorreoNuevoUsuario, editTextContrasenyaNuevoUsuario, editTextContrasenya2NuevoUsuario;
    private String correo, contrasenya, contrasenya2;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);


        //*******************************--REFERENCIAS--********************************************
        botonCrearUsuario = findViewById(R.id.botonCrearUsuario);
        editTextCorreoNuevoUsuario = findViewById(R.id.editTextCorreoNuevoUsuario);
        editTextContrasenyaNuevoUsuario= findViewById(R.id.editTextContrasenyaNuevoUsuario);
        editTextContrasenya2NuevoUsuario=findViewById(R.id.editTextContrasenya2NuevoUsuario);
        //******************************************************************************************

        //*******************************--EVENTOS--************************************************
        botonCrearUsuario.setOnClickListener(this);
        //******************************************************************************************

        //instanciamos firebase
        mAuth = FirebaseAuth.getInstance();



    }

    @Override
    public void onClick(View v) {
        editTextCorreoNuevoUsuario.getText();

        correo = editTextCorreoNuevoUsuario.getText().toString();
        contrasenya = editTextContrasenyaNuevoUsuario.getText().toString();
        contrasenya2 = editTextContrasenya2NuevoUsuario.getText().toString();

        //si los campos no están vacíos
        if(!(correo.equals("") || contrasenya.equals("") || contrasenya2.equals(""))){
            if(contrasenya.equals(contrasenya2)){
                //llamamos al método que hace crea usuario
                creaUsuario(correo,contrasenya);

            }else{
                Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                editTextContrasenyaNuevoUsuario.setText(null);
                editTextContrasenya2NuevoUsuario.setText(null);
            }
        }else{
            Toast.makeText(this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }
    public void creaUsuario(String correo, String contrasenya){

        mAuth.createUserWithEmailAndPassword(correo, contrasenya).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                           //el usuario se ha creado con éxito
                            Toast.makeText(NuevoUsuario.this, "Usuario creado con éxito", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();

                            editTextCorreoNuevoUsuario.setText(null);
                            editTextContrasenyaNuevoUsuario.setText(null);
                            editTextContrasenya2NuevoUsuario.setText(null);

                            //volvemos al login
                            Intent i = new Intent(NuevoUsuario.this, Login.class);
                            startActivity(i);

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(NuevoUsuario.this, "Error al crear usuario, revise el formato de correo o la contraseña.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });
    }
}
