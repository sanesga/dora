package com.example.sandra.dora.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.sandra.dora.R;
import com.example.sandra.dora.controller.UserController;
import com.example.sandra.dora.fragments.fragment_almacenamientos;
import com.example.sandra.dora.pojos.Lugar;

public class VerAlmacenamientos extends AppCompatActivity {

    private fragment_almacenamientos fragment_detalle_lugares;
    private Lugar l;
    private Toolbar toolbar;
    private UserController uc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_lugares);

        fragment_detalle_lugares = (fragment_almacenamientos) getSupportFragmentManager().findFragmentById(R.id.fragmentDetalle);

        //----------TOOLBAR----------
        setSupportActionBar(toolbar);
        //--------------------------

        //recogemos del intent de ver lugares, el lugar l
        Bundle parametros = this.getIntent().getExtras();
        if (parametros != null) {
            l = (Lugar) getIntent().getExtras().get("lugar");
        }
        fragment_detalle_lugares.mostrarDetalle(l);
    }

    public void actionMenu(View view) {

        Intent i = null;

        switch (view.getTag().toString()) {
            case "ajustes":
                i = new Intent(this, Configuracion.class);
                break;
            case "user":
                //aqui llamaremos al metodo getUser y obtendremos el nombre del usuario
                uc = new UserController();
                String userMail;
                userMail = uc.getUserData();
                i = new Intent(this, Account.class);
                i.putExtra("mailAccount", userMail);
                break;
            case "home":
                i = new Intent(this, MenuPrincipal.class);
                break;
        }
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, MenuPrincipal.class);
        startActivity(i);
        finish();

    }
}
