package com.example.sandra.dora.adaptador;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.sandra.dora.R;
import com.example.sandra.dora.pojos.Almacenamiento;
import com.example.sandra.dora.pojos.Lugar;
import com.example.sandra.dora.pojos.Objeto;

import java.util.ArrayList;


public class MiArrayAdapterGridViewObjetos<T> extends ArrayAdapter<T> {

    public MiArrayAdapterGridViewObjetos(Context context, ArrayList<T> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listItemView = convertView;

        if (listItemView == null) {
            listItemView = inflater.inflate(R.layout.layout_gridview_objects_result_search, parent, false);
        }
        //REFERENCIAS*********************************************************
        ImageView image = listItemView.findViewById(R.id.ivObject);
        TextView objectName= listItemView.findViewById(R.id.tvNameObject);
        TextView originPlace = listItemView.findViewById(R.id.tvOriginPlace);
        TextView originStorage = listItemView.findViewById(R.id.tvOriginStorage);
        //*********************************************************************

        Objeto o = (Objeto)getItem(position); //obtenemos lugar seleccionado

        objectName.setText(o.getNombre()); //seteamos nombre
        originPlace.setText(o.getLugar());
        originStorage.setText(o.getAlmacenamiento());

        //pasamos la foto del objeto en formato base64 (string) a bitmap para mostrarla en el imageView
        Bitmap decodedByte=o.decodeImage();
        image.setImageBitmap(decodedByte);

        return listItemView;
    }
}

