package com.example.sandra.dora.fragments;


import com.example.sandra.dora.pojos.Lugar;

//interfaz lugares listener
public interface LugaresListener {
    void onLugarSeleccionado(Lugar l);
}
